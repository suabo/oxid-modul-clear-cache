<div class="clearcachepopup" style="position:absolute;top:100px;left:50%;margin-left:-225px;width:450px;padding:10px;border:3px solid #ccc;border-radius:5px;background:#fff;color:#666;box-shadow:3px 3px 3px 0 #666;">
  <h1 style="font-size:18px;font-family:Verdana;color:#666;border-bottom:3px solid #ccc;padding:10px;margin:-10px -10px 0 -10px;background:lightgreen;">
    [{ oxmultilang ident="SUABOCLEARCACHE_CLEARCACHE_HEADLINE" }]
  </h1>
  <span style="font-size:32px;color:lightgreen;font-weight:bold;padding-right:10px;">&#10003;</span> [{ oxmultilang ident="SUABOCLEARCACHE_CLEARCACHE_TEXT" }]
  [{if $clearcachelog}]
  <div class="details" style="height:50px;width:400px;overflow-y:auto;">
    [{$clearcachelog}]
  </div>  
  [{/if}]
</div>