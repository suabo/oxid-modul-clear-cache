<?php

$sLangName  = "English";
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(
    'charset'                             => 'UTF-8',
    'mxsuaboclearcache'                   => 'Clear Shop Cache',    
    'SUABOCLEARCACHE_BUTTON'              => 'Clear Shopcache',
    'SUABOCLEARCACHE_CLEARCACHE_HEADLINE' => 'Shopcache empty',
    'SUABOCLEARCACHE_CLEARCACHE_TEXT'     => 'The Shopcache is empty.',
    'SUABOCLEARCACHE_TIPP_HEADLINE'       => 'Clear cache faster',
    'SUABOCLEARCACHE_TIPP_TEXT'           => 'If you have to clear the cache more often it\'s good to have the function
      right available. So there is a way to easily implement a link in the main navigationbar at the top of the admin backend.
      Therefore we predefined a smarty block, which you can see in admin backend >> systemhealth >> missing template blocks.
      To show this link you have to wrap following code around the existing links in /application/views/admin/tpl/header.tpl',
    'SUABOCLEARCACHE_TIPP_TEXT2'          => 'After the changes it should look like this',
    'SUABOCLEARCACHE_TIPP_TEXT3'          => 'If you didn\'t change the admin templates, then you can easily copy content from folder change_this to your shop root path to add the missing template block.',       
);

/*
[{ oxmultilang ident="GENERAL_YOUWANTTODELETE" }]
*/
