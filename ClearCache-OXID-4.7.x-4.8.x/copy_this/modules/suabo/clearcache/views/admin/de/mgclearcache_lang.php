<?php

$sLangName  = "Deutsch";
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(
    'charset'                             => 'UTF-8',
    'mxsuaboclearcache'                   => 'Temporäre Shopdateien entfernen',
    'SUABOCLEARCACHE_BUTTON'              => 'Tempräre Dateien löschen',
    'SUABOCLEARCACHE_CLEARCACHE_HEADLINE' => 'Temporären Shopdateien gelöscht',
    'SUABOCLEARCACHE_CLEARCACHE_TEXT'     => 'Ihre Temporären Shopdateien wurden erfolgreich gelöscht.',
    'SUABOCLEARCACHE_TIPP_HEADLINE'       => 'Noch schneller den Cache leeren',
    'SUABOCLEARCACHE_TIPP_TEXT'           => 'Wenn Sie öfters den Cache löschen müssen können Sie ganz einfach in den Button oben in der Leiste (in welcher auch Home, Startseite ... und Abmelden steht) anzeigen.
      Hierzu ist bereits ein entsprechender Templateblock im Modul enthalten, was Sie unter Systemgesundheit (Fehlende Modulblöcke im Template) sehen können. 
      Um den Button anzuzeigen muss der Smarty Block im Admin Template zunächst erstellt werden. Öffnen Sie die Datei /application/views/admin/tpl/header.tpl und fügen 
      folgenden Codeblock um die vorhandenen Buttons ein',
    'SUABOCLEARCACHE_TIPP_TEXT2'          => 'Das Ergebnis ihrer Anpassung sollte ungefähr so aussehen',
    'SUABOCLEARCACHE_TIPP_TEXT3'          => 'Wenn Sie keine Anpassungen an den Admin Templates durchgeführt haben können Sie auch einfach den Inhalt des Ordners change_this in ihr Shop root Verzeichnis kopieren um den Fehlenden Templateblock einzufügen.',
);

/*
[{ oxmultilang ident="GENERAL_YOUWANTTODELETE" }]
*/
